package com.example.a300assigment.home;

public class Event {
    int image, background;
    String title;

  

    public Event(int image, String title, int background) {
        this.image = image;
        this.background = background;
        this.title = title;
    }

    public int getImage() {
        return image;
    }

    public String getTitle() {
        return title;
    } 
    
    public int getBackground() {
        return background;
    }
}
