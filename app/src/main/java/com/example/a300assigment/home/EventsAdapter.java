package com.example.a300assigment.home;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.a300assigment.R;

import java.util.ArrayList;

public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.newsviewHolder> {

    ArrayList<Event> events;


    public EventsAdapter(ArrayList<Event> events) {
        this.events = events;
    }

    @NonNull
    @Override
    public newsviewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //set the layout id
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.homepage_events_card_design,parent,false);
        newsviewHolder newsviewholder = new newsviewHolder(view);

        return newsviewholder;
    }

    @Override
    public void onBindViewHolder(@NonNull newsviewHolder holder, int position) {
        Event newsHelperClass = events.get(position);

        //set the homepage-news-card image, title, background
        holder.image.setImageResource(newsHelperClass.getImage());
        holder.title.setText(newsHelperClass.getTitle());
        holder.bg.setBackgroundResource(newsHelperClass.getBackground());




    }

    @Override
    public int getItemCount() {
        return events.size();
    }


    public static class newsviewHolder extends RecyclerView.ViewHolder{

        ImageView image;
        TextView title;
        RelativeLayout bg;

        public newsviewHolder(@NonNull View itemView) {
            super(itemView);


            //find the item id
            image = itemView.findViewById(R.id.new_image);
            title = itemView.findViewById(R.id.new_title);
            bg = itemView.findViewById(R.id.new_color);


        }
    }

}
