package com.example.a300assigment.home;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.a300assigment.R;

import java.util.ArrayList;

public class HomepageFragment extends Fragment {

    private RecyclerView eventRecycler, newRecycler;
    private RecyclerView.Adapter adapter, adapter2;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view=  inflater.inflate(R.layout.fragment_homepage,container, false);
        newRecycler  = view.findViewById(R.id.news_recycler);
        eventRecycler = view.findViewById(R.id.event_recycler);
        //Call Adapter News product
        NewRecycler();

        //Call Adapter Event
        EventRecycler();



        return view;
    }

    private void EventRecycler() {
        eventRecycler.setHasFixedSize(true);
        eventRecycler.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL,false));

        ArrayList<Event> events = new ArrayList<>();
        //Add data
        events.add(new Event(R.drawable.discount,getResources().getString(R.string.discount),R.drawable.card_1));
        events.add(new Event(R.drawable.buy2get1,getResources().getString(R.string.buy2get1),R.drawable.card_2));

        adapter2 = new EventsAdapter(events);
        eventRecycler.setAdapter(adapter2);



    }

    private void NewRecycler() {
        newRecycler.setHasFixedSize(true);
        newRecycler.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL,false));

        ArrayList<News> news = new ArrayList<>();
        //Add data
        news.add(new News(R.drawable.boyclothe,getResources().getString(R.string.boyclothes),getResources().getString(R.string.good)));
        news.add(new News(R.drawable.girldress,getResources().getString(R.string.girldress),getResources().getString(R.string.good)));
        news.add(new News(R.drawable.toybear,getResources().getString(R.string.toybear),getResources().getString(R.string.good)));

        adapter = new NewsAdapter(news);
        newRecycler.setAdapter(adapter);




    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}