package com.example.a300assigment.home;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class EventTest {

    @Test
    public void constructor() {
        Event event = new Event(123,"title",456);

        Assert.assertEquals(event.getImage(),123);
        Assert.assertEquals(event.getTitle(),"title");
        Assert.assertEquals(event.getBackground(),456);
    }

}