package com.example.a300assigment.home;

import com.example.a300assigment.Sign.User;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class NewsTest {

    @Test
    public void constructor() {
        News event = new News(123,"title","des");

        Assert.assertEquals(event.getImage(),123);
        Assert.assertEquals(event.getTitle(),"title");
        Assert.assertEquals(event.getDescription(),"des");
    }


}